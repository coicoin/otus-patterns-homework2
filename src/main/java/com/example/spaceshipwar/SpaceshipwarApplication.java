package com.example.spaceshipwar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpaceshipwarApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpaceshipwarApplication.class, args);
  }

}
