package com.example.spaceshipwar.model;

import com.example.spaceshipwar.constants.CommonFields;
import com.example.spaceshipwar.utils.UObject;
import com.example.spaceshipwar.utils.UObjectImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Spaceship {
  /**
   * position is the current coordinates x, y of the flying vehicle
   * ex. (12, 5)
   */
  private Vector position;
  /**
   * direction can receive number from 0 ... 8
   */
  private int direction;
  /**
   * directionsNumber max = 8, it means 360 / 8 = 45 degrees
   */
  private int directionsNumber = 8;
  /**
   * Rotation degree where 45 angularVelocity eq. 1 directionsNumber
   * Formula (angularVelocity / 45) % 8
   */
  private int angularVelocity;
  /**
   * velocity is the speed
   * ex. 5
   */
  private int velocity;
  UObject uObject = new UObjectImpl();

  public Spaceship(Vector position) {
    this.position = position;
    this.uObject.setProperty(CommonFields.POSITION, position);
  }

  public Spaceship(Vector position, int direction, int directionsNumber, int angularVelocity, int velocity) {
    this.position = position;
    this.direction = direction;
    this.directionsNumber = directionsNumber;
    this.angularVelocity = angularVelocity;
    this.velocity = velocity;
    this.uObject.setProperty(CommonFields.POSITION, position);
    this.uObject.setProperty(CommonFields.DIRECTION, direction);
    this.uObject.setProperty(CommonFields.DIRECTIONS_NUMBER, directionsNumber);
    this.uObject.setProperty(CommonFields.ANGULAR_VELOCITY, angularVelocity);
    this.uObject.setProperty(CommonFields.VELOCITY, velocity);
  }

  public void setPosition(Vector position) {
    this.position = position;
    uObject.setProperty(CommonFields.POSITION, position);
  }

  public void setDirection(int direction) {
    this.direction = direction;
    uObject.setProperty(CommonFields.DIRECTION, direction);
  }

  public void setDirectionsNumber(int directionsNumber) {
    this.directionsNumber = directionsNumber;
    uObject.setProperty(CommonFields.DIRECTIONS_NUMBER, directionsNumber);
  }

  public void setAngularVelocity(int angularVelocity) {
    this.angularVelocity = angularVelocity;
    uObject.setProperty(CommonFields.ANGULAR_VELOCITY, angularVelocity);
  }

  public void setVelocity(int velocity) {
    this.velocity = velocity;
    uObject.setProperty(CommonFields.VELOCITY, velocity);
  }
}