package com.example.spaceshipwar.service;

public interface Rotatable {
  int getDirection();

  void setDirection(int newV);

  int getAngularVelocity();

  int getDirectionsNumber();

  void execute();
}
