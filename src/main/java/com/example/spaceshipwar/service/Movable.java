package com.example.spaceshipwar.service;

import com.example.spaceshipwar.model.Vector;

public interface Movable {
  Vector getPosition();

  void setPosition(Vector newV);

  Vector getVelocity();

  void execute();
}
