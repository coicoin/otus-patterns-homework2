package com.example.spaceshipwar.constants;

public interface CommonFields {
  String POSITION = "Position";
  String DIRECTION = "Direction";
  String DIRECTIONS_NUMBER = "DirectionsNumber";
  String ANGULAR_VELOCITY = "AngularVelocity";
  String VELOCITY = "Velocity";
}
