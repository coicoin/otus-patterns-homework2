package com.example.spaceshipwar.adapter;

import static com.example.spaceshipwar.utils.Validator.WRONG_DATA_TYPE;
import static com.example.spaceshipwar.utils.Validator.validateVector;

import com.example.spaceshipwar.constants.CommonFields;
import com.example.spaceshipwar.model.Vector;
import com.example.spaceshipwar.service.Movable;
import com.example.spaceshipwar.utils.UObject;
import lombok.ToString;

@ToString
public class MovableAdapter implements Movable {

  UObject o;

  public MovableAdapter(UObject o) {
    this.o = o;
  }

  @Override
  public Vector getPosition() {
    if (!(o.getProperty(CommonFields.POSITION) instanceof Vector)) {
      throw new ClassCastException(WRONG_DATA_TYPE + " " + CommonFields.POSITION);
    } else {
      validateVector((Vector) o.getProperty(CommonFields.POSITION));
    }
    return (Vector) o.getProperty(CommonFields.POSITION);
  }

  @Override
  public void setPosition(Vector newV) {
    o.setProperty(CommonFields.POSITION, newV);
  }

  public void execute() {
    o.setProperty(CommonFields.POSITION, plus(getPosition(), getVelocity()));
  }

  /**
   * Get speed as coordinates x, y. Ex. (-7, 3)
   */
  @Override
  public Vector getVelocity() {
    if (!(o.getProperty(CommonFields.DIRECTION) instanceof Integer)
        || !(o.getProperty(CommonFields.DIRECTIONS_NUMBER) instanceof Integer)
        || !(o.getProperty(CommonFields.VELOCITY) instanceof Integer)) {
      throw new ClassCastException(WRONG_DATA_TYPE);
    }
    int d = (int) o.getProperty(CommonFields.DIRECTION);
    int n = (int) o.getProperty(CommonFields.DIRECTIONS_NUMBER);
    int v = (int) o.getProperty(CommonFields.VELOCITY);
    return new Vector(v * Math.cos(d / 360d * n), v * Math.sin(d / 360d * n));
  }

  public Vector plus(Vector position, Vector velocity) {
    return new Vector(position.getX() + velocity.getX(), position.getY() + velocity.getY());
  }
}