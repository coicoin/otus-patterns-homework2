package com.example.spaceshipwar.adapter;

import static com.example.spaceshipwar.utils.Validator.WRONG_DATA_TYPE;

import com.example.spaceshipwar.constants.CommonFields;
import com.example.spaceshipwar.service.Rotatable;
import com.example.spaceshipwar.utils.UObject;
import lombok.ToString;

@ToString
public class RotatableAdapter implements Rotatable {

  public static final int MIN_ANGLE_STEP = 45;
  public static final int FULL_ROTATION = 360;
  public static final int MAX_ANGLES_QUANTITY = 8;

  UObject o;

  public RotatableAdapter(UObject o) {
    this.o = o;
  }

  @Override
  public int getDirection() {
    if (!(o.getProperty(CommonFields.DIRECTION) instanceof Integer)) {
      throw new ClassCastException(WRONG_DATA_TYPE + " " + CommonFields.DIRECTION);
    }
    return (int) o.getProperty(CommonFields.DIRECTION);
  }

  @Override
  public void setDirection(int newV) {
    o.setProperty(CommonFields.DIRECTION, newV);
  }

  @Override
  public int getAngularVelocity() {
    if (!(o.getProperty(CommonFields.ANGULAR_VELOCITY) instanceof Integer)) {
      throw new ClassCastException(WRONG_DATA_TYPE + " " + CommonFields.ANGULAR_VELOCITY);
    }
    int angularVelocity = (int) o.getProperty(CommonFields.ANGULAR_VELOCITY);
    if (angularVelocity >= 0 && angularVelocity % MIN_ANGLE_STEP == 0) {
      if (angularVelocity >= FULL_ROTATION) {
        return MAX_ANGLES_QUANTITY;
      } else {
        return (angularVelocity / MIN_ANGLE_STEP) % MAX_ANGLES_QUANTITY;
      }
    } else {
      return 0;
    }
  }

  @Override
  public int getDirectionsNumber() {
    if (!(o.getProperty(CommonFields.DIRECTIONS_NUMBER) instanceof Integer)) {
      throw new ClassCastException(WRONG_DATA_TYPE + " " + CommonFields.DIRECTIONS_NUMBER);
    }
    return (int) o.getProperty(CommonFields.DIRECTIONS_NUMBER);
  }

  public void execute() {
    o.setProperty(CommonFields.DIRECTION, (getDirection() + getAngularVelocity()) % getDirectionsNumber());
  }

}