package com.example.spaceshipwar.utils;

import static java.lang.Double.isFinite;

import com.example.spaceshipwar.constants.CommonFields;
import com.example.spaceshipwar.model.Vector;

public class Validator {

  public static final String COORDINATES_CANNOT_BE_NULL_NAN_OR_INFINITE
      = "Coordinates cannot receive values as like null, NaN, INF, -INF";
  public static final String MAX_COORDINATE_ACHIEVED = "Maximum coordinate value achieved";
  public static final String MIN_COORDINATE_ACHIEVED = "Minimum coordinate value achieved";
  public static final String WRONG_DATA_TYPE = "Data type is wrong";

  public static void validateUObject(UObject uObject) {
    if (!(uObject.getProperty(CommonFields.POSITION) instanceof Vector)) {
      throw new ClassCastException(WRONG_DATA_TYPE);
    } else {
      validateVector((Vector) uObject.getProperty(CommonFields.POSITION));
    }
    if (!(uObject.getProperty(CommonFields.VELOCITY) instanceof Integer)
        || !(uObject.getProperty(CommonFields.DIRECTION) instanceof Integer)
        || !(uObject.getProperty(CommonFields.DIRECTIONS_NUMBER) instanceof Integer)
        || !(uObject.getProperty(CommonFields.ANGULAR_VELOCITY) instanceof Integer)) {
      throw new ClassCastException(WRONG_DATA_TYPE);
    }
  }

  public static void validateVector(Vector vector) {
    if (vector.getX() == null || vector.getY() == null || !isFinite(vector.getX()) || !isFinite(vector.getY())) {
      throw new IllegalArgumentException(COORDINATES_CANNOT_BE_NULL_NAN_OR_INFINITE);
    }
    if (vector.getX() >= Double.MAX_VALUE || vector.getY() >= Double.MAX_VALUE) {
      throw new IllegalStateException(MAX_COORDINATE_ACHIEVED);
    }
    if (vector.getX() == Double.MIN_VALUE || vector.getY() == Double.MIN_VALUE) {
      throw new IllegalStateException(MIN_COORDINATE_ACHIEVED);
    }
  }
}
