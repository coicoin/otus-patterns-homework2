package com.example.spaceshipwar.utils;

import java.util.HashMap;
import java.util.Map;
import lombok.ToString;

@ToString
public class UObjectImpl implements UObject {

  Map<String, Object> uObject = new HashMap<>();

  @Override
  public Object getProperty(String key) {
    return uObject.get(key);
  }

  @Override
  public void setProperty(String key, Object newValue) {
    uObject.put(key, newValue);
  }
}
