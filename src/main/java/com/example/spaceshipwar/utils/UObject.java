package com.example.spaceshipwar.utils;

public interface UObject {
  Object getProperty(String key);
  void setProperty(String key, Object newValue);
}
