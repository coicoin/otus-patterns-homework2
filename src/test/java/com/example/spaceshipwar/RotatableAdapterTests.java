package com.example.spaceshipwar;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.spaceshipwar.adapter.RotatableAdapter;
import com.example.spaceshipwar.constants.CommonFields;
import com.example.spaceshipwar.model.Spaceship;
import com.example.spaceshipwar.service.Rotatable;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RotatableAdapterTests {

  @Test
  void testRotatableAdapter_incorrectDataTypeDirection() {
    Spaceship spaceship = new Spaceship();
    Rotatable rotatingObject = Mockito.spy(new RotatableAdapter(spaceship.getUObject()));

    ClassCastException exception = assertThrows(ClassCastException.class, rotatingObject::execute);
    assertTrue(exception.getMessage().contains(CommonFields.DIRECTION));
  }

  @Test
  void testRotatableAdapter_incorrectDataTypeAngularVelocity() {
    Spaceship spaceship = new Spaceship();
    spaceship.getUObject().setProperty(CommonFields.DIRECTION, 1);
    spaceship.getUObject().setProperty(CommonFields.DIRECTIONS_NUMBER, 8);
    Rotatable rotatingObject = Mockito.spy(new RotatableAdapter(spaceship.getUObject()));

    ClassCastException exception = assertThrows(ClassCastException.class, rotatingObject::execute);
    assertTrue(exception.getMessage().contains(CommonFields.ANGULAR_VELOCITY));
  }

  @Test
  void testRotatableAdapter_incorrectDataTypeDirectionNumber() {
    Spaceship spaceship = new Spaceship();
    spaceship.getUObject().setProperty(CommonFields.DIRECTION, 1);
    spaceship.getUObject().setProperty(CommonFields.ANGULAR_VELOCITY, 45);
    Rotatable rotatingObject = Mockito.spy(new RotatableAdapter(spaceship.getUObject()));

    ClassCastException exception = assertThrows(ClassCastException.class, rotatingObject::execute);
    assertTrue(exception.getMessage().contains(CommonFields.DIRECTIONS_NUMBER));
  }

  /**
   * Task 8.3
   */
  @Test
  void testRotatableAdapterChangeDirection() {
    Spaceship spaceship = new Spaceship();
    spaceship.getUObject().setProperty(CommonFields.DIRECTION, 1);
    spaceship.getUObject().setProperty(CommonFields.ANGULAR_VELOCITY, 45);
    spaceship.getUObject().setProperty(CommonFields.DIRECTIONS_NUMBER, 8);
    Rotatable rotatingObject = Mockito.spy(new RotatableAdapter(spaceship.getUObject()));

    Assertions.assertEquals(1, rotatingObject.getDirection());

    rotatingObject.execute();
    Assertions.assertEquals(2, rotatingObject.getDirection());

    rotatingObject.execute();
    Assertions.assertEquals(3, rotatingObject.getDirection());
  }

}