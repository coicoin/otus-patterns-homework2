package com.example.spaceshipwar;

import static com.example.spaceshipwar.utils.Validator.COORDINATES_CANNOT_BE_NULL_NAN_OR_INFINITE;
import static com.example.spaceshipwar.utils.Validator.MAX_COORDINATE_ACHIEVED;
import static com.example.spaceshipwar.utils.Validator.MIN_COORDINATE_ACHIEVED;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.spaceshipwar.adapter.MovableAdapter;
import com.example.spaceshipwar.constants.CommonFields;
import com.example.spaceshipwar.model.Spaceship;
import com.example.spaceshipwar.model.Vector;
import com.example.spaceshipwar.service.Movable;
import com.example.spaceshipwar.utils.Validator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MovableAdapterTests {

  private static final Double NAN = Double.longBitsToDouble(0x7ff8000000000000L);
  private static final Double NEGATIVE_INFINITY = Double.longBitsToDouble(0xfff0000000000000L);
  private static final Double POSITIVE_INFINITY = Double.longBitsToDouble(0x7ff0000000000000L);

  Spaceship spaceship;

  @BeforeEach
  void setUp() {
    spaceship = new Spaceship();
  }

  /**
   * Task 7.3.1
   * Для объекта, находящегося в точке (12, 5) и движущегося со скоростью (-7, 3)
   * движение меняет положение объекта на (5, 8)
   */
  @Test
  void testMovableAdapterPlusCalculation() {
    Vector expectedVector = new Vector(5d, 8d);
    Vector velocityVector = new Vector(-7d, 3d);
    Vector positionVector = new Vector(12d, 5d);
    spaceship.getUObject().setProperty(CommonFields.DIRECTION, 0);
    spaceship.getUObject().setProperty(CommonFields.DIRECTIONS_NUMBER, 0);
    spaceship.getUObject().setProperty(CommonFields.VELOCITY, 0);
    Movable movingObject = Mockito.spy(new MovableAdapter(spaceship.getUObject()));

    movingObject.setPosition(positionVector);
    Assertions.assertEquals(positionVector, movingObject.getPosition());

    Mockito.when(movingObject.getVelocity()).thenReturn(velocityVector);
    movingObject.execute();
    Assertions.assertEquals(expectedVector, movingObject.getPosition());
  }

  /**
   * Task 7.3.2
   * Попытка сдвинуть объект, у которого невозможно прочитать положение в пространстве, приводит к ошибке
   */
  @Test
  void testMovableAdapter_getPositionError() {
    Movable movingObject = Mockito.spy(new MovableAdapter(spaceship.getUObject()));
    movingObject.setPosition(new Vector(NAN, null));
    IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
        () -> Validator.validateVector(movingObject.getPosition()));

    assertTrue(exception.getMessage().contains(COORDINATES_CANNOT_BE_NULL_NAN_OR_INFINITE));
  }

  @Test
  void testMovableAdapter_incorrectDataTypePosition() {
    Spaceship spaceship = new Spaceship();
    spaceship.getUObject().setProperty(CommonFields.DIRECTION, 1);
    spaceship.getUObject().setProperty(CommonFields.DIRECTIONS_NUMBER, 8);
    spaceship.getUObject().setProperty(CommonFields.VELOCITY, 5);
    Movable movingObject = Mockito.spy(new MovableAdapter(spaceship.getUObject()));

    ClassCastException exception = assertThrows(ClassCastException.class, movingObject::execute);
    assertTrue(exception.getMessage().contains(CommonFields.POSITION));
  }

  /**
   * Task 7.3.3
   * Попытка сдвинуть объект, у которого невозможно прочитать значение мгновенной скорости, приводит к ошибке
   */
  @Test
  void testMovableAdapter_getVelocityError() {
    spaceship.getUObject().setProperty(CommonFields.DIRECTION, 1);
    spaceship.getUObject().setProperty(CommonFields.DIRECTIONS_NUMBER, 8);
    spaceship.getUObject().setProperty(CommonFields.VELOCITY, 1);
    Movable movingObject = Mockito.spy(new MovableAdapter(spaceship.getUObject()));
    Mockito.when(movingObject.getVelocity()).thenReturn(new Vector(NEGATIVE_INFINITY, POSITIVE_INFINITY));
    IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
        () -> Validator.validateVector(movingObject.getVelocity()));

    assertTrue(exception.getMessage().contains(COORDINATES_CANNOT_BE_NULL_NAN_OR_INFINITE));
  }

  @Test
  void testRotatableAdapter_incorrectDataTypeVelocity() {
    Spaceship spaceship = new Spaceship();
    spaceship.getUObject().setProperty(CommonFields.DIRECTION, 1);
    spaceship.getUObject().setProperty(CommonFields.DIRECTIONS_NUMBER, 8);
    Movable movingObject = Mockito.spy(new MovableAdapter(spaceship.getUObject()));

    assertThrows(ClassCastException.class, movingObject::execute);
  }

  /**
   * Task 7.3.4
   * Попытка сдвинуть объект, у которого невозможно изменить положение в пространстве, приводит к ошибке
   */
  @Test
  void testMovableAdapter_changePositionError() {
    spaceship.getUObject().setProperty(CommonFields.POSITION, new Vector(0d, 0d));
    Movable movingObject = Mockito.spy(new MovableAdapter(spaceship.getUObject()));
    Mockito.when(movingObject.getPosition()).thenReturn(new Vector(Double.MAX_VALUE, 0d));
    IllegalStateException exceptionMax = assertThrows(IllegalStateException.class,
        () -> Validator.validateVector(movingObject.getPosition()));

    Mockito.when(movingObject.getPosition()).thenReturn(new Vector(0d, Double.MIN_VALUE));
    IllegalStateException exceptionMin = assertThrows(IllegalStateException.class,
        () -> Validator.validateVector(movingObject.getPosition()));

    assertTrue(exceptionMax.getMessage().contains(MAX_COORDINATE_ACHIEVED));
    assertTrue(exceptionMin.getMessage().contains(MIN_COORDINATE_ACHIEVED));
  }

  @Test
  void testRotatableAdapter_incorrectDataTypeDirection() {
    Spaceship spaceship = new Spaceship();
    Movable movingObject = Mockito.spy(new MovableAdapter(spaceship.getUObject()));

    assertThrows(ClassCastException.class, movingObject::execute);
  }

  @Test
  void testRotatableAdapter_incorrectDataTypeDirectionNumber() {
    Spaceship spaceship = new Spaceship();
    spaceship.getUObject().setProperty(CommonFields.DIRECTION, 1);
    spaceship.getUObject().setProperty(CommonFields.VELOCITY, 5);
    Movable movingObject = Mockito.spy(new MovableAdapter(spaceship.getUObject()));

    assertThrows(ClassCastException.class, movingObject::execute);
  }

}