package com.example.spaceshipwar;

import static com.example.spaceshipwar.utils.Validator.WRONG_DATA_TYPE;
import static com.example.spaceshipwar.utils.Validator.validateUObject;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.example.spaceshipwar.constants.CommonFields;
import com.example.spaceshipwar.model.Vector;
import com.example.spaceshipwar.utils.UObject;
import com.example.spaceshipwar.utils.UObjectImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UObjectTests {

  @Test
  void testCreateUObject() {
    String expectedObject =
        "UObjectImpl(uObject={DirectionsNumber=8, Position=Vector(x=1.0, y=1.0), AngularVelocity=45, Velocity=1, "
            + "Direction=1})";
    UObject uObject = new UObjectImpl();
    uObject.setProperty(CommonFields.POSITION, new Vector(1d, 1d));
    uObject.setProperty(CommonFields.VELOCITY, 1);
    uObject.setProperty(CommonFields.DIRECTION, 1);
    uObject.setProperty(CommonFields.DIRECTIONS_NUMBER, 8);
    uObject.setProperty(CommonFields.ANGULAR_VELOCITY, 45);

    Assertions.assertEquals(expectedObject, uObject.toString());
  }

  @Test
  void testCreateUObject_incorrectDataType() {
    UObject uObject = new UObjectImpl();
    uObject.setProperty(CommonFields.POSITION, "test");
    uObject.setProperty(CommonFields.VELOCITY, "test");
    uObject.setProperty(CommonFields.DIRECTION, "test");
    uObject.setProperty(CommonFields.DIRECTIONS_NUMBER, "test");
    uObject.setProperty(CommonFields.ANGULAR_VELOCITY, "test");

    ClassCastException exception = assertThrows(ClassCastException.class, () -> validateUObject(uObject));
    assertTrue(exception.getMessage().contains(WRONG_DATA_TYPE));
  }

}